import { configureStore } from '@reduxjs/toolkit';
import PokemonReducer from '../features/pokemon/PokemonSlice';
import PokemonTypeReducer from '../features/pokemon/PokemonTypeSlice';

export default configureStore({
  reducer: {
    pokemon: PokemonReducer,
    pokemonType: PokemonTypeReducer
  },
});
