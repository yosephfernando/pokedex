import { endpoints } from './endPontCollection';

export default async function getPokemonTypeApi(){
    let response = await fetch(endpoints.type).catch(e => {
        console.log("ERROR >>", e);
    });

    response = await response.json();
    return response;
}