import { endpoints } from './endPontCollection';

export default async function getPokemonCharacter(limit, offset){
    let response = await fetch(endpoints.pokemon+`?limit=${limit}&offset=${offset}`).catch(e => {
        console.log("ERROR >>", e);
    });

    response = await response.json();
    return response;
}

export async function getPokemonCharacterByType(type){
    let response = await fetch(endpoints.type+type).catch(e => {
        console.log("ERROR >>", e);
    });

    response = await response.json();
    return response;
}