import { endpoints } from './endPontCollection';

export default async function getPokemonAbilities(pokemonName){
    let response = await fetch(endpoints.pokemon+pokemonName).catch(e => {
        console.log("ERROR >>", e);
    });

    response = await response.json();
    return response;
}

export async function getPokemonAbilitiesEfect(abilityName){
    let response = await fetch(endpoints.abilities+abilityName).catch(e => {
        console.log("ERROR >>", e);
    });

    response = await response.json();
    return response;
}