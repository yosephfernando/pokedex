const baseUrl = "https://pokeapi.co/api/v2/"

export const endpoints = {
    "pokemon": baseUrl+"pokemon/",
    "type": baseUrl+"type/",
    "abilities": baseUrl+"ability/"
};