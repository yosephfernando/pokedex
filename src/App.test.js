import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import store from './app/store';
import App from './App';
import getPokemonCharacter from '../src/apiRequest/getPokemonCharacter';
import getPokemonAbilities from '../src/apiRequest/getPokemonAbilities';
import reducer from './features/pokemon/PokemonSlice';

test('renders home page', () => {
  const { getByText } = render(
    <Provider store={store}>
      <App />
    </Provider>
  );

  expect(getByText('Pokedex')).toBeInTheDocument();
});

test('api fetch 10 data pokemon', async () => {
  let pokemonData = await getPokemonCharacter(10,10)
  expect(pokemonData).toHaveProperty('results');
});

test('api fetch data pokemon abilities', async () => {
  let pokemonAbilitiesData = await getPokemonAbilities('metapod')
  expect(pokemonAbilitiesData).toHaveProperty('abilities');
});