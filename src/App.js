import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Home from './pages/home';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';


const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  // drawer: {
  //   width: drawerWidth,
  //   flexShrink: 0,
  // },
  // drawerPaper: {
  //   width: drawerWidth,
  // },
  // drawerContainer: {
  //   overflow: 'auto',
  // },
  content: {
    flexGrow: 1,
    padding: theme.spacing(6),
  },
  link: {
    textDecoration:'none'
  }
}));

function App() {
  const classes = useStyles();
  const pages = [
    {"title":"List views", "link":"/home"}
  ]
  return (
    <Router>
        <div className={classes.root}>
          <CssBaseline />
          <AppBar position="fixed" className={classes.appBar}>
            <Toolbar>
              <Typography variant="h6" noWrap>
                 Pokedex
              </Typography>
            </Toolbar>
          </AppBar>
          <main className={classes.content}>
            <Toolbar />
            <Switch>
              <Route path="/home">
                <Home />
              </Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </main>
        </div>
    </Router>
  );
}

export default App;
