import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { fetchPokemon, pokemonSelector, insertPockemon } from './PokemonSlice';
import { fetchPokemonType, pokemonTypeSelector } from './PokemonTypeSlice';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Button from '@material-ui/core/Button';
import FullScreenDialog from './PokemonDialog';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme) => ({
    root: {
        textAlign:"center"
    },
    bottomF: {
        marginBottom:"5px"
    },
    thumbBorder: {
        borderLeftStyle: "solid",
        borderWLeftidth: "1px",
        borderLeftColor:"#3f51b5"
    },
    formControl: {
        minWidth: 150,
        marginBottom: 20
    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
}));

export function Pokemon(props) {
    const dispatch = useDispatch();
    const classes = useStyles();
    const { data, dataByType, loading } = useSelector(pokemonSelector);
    const { dataType, loadingType } = useSelector(pokemonTypeSelector);
    const perLoad = 10;
    const [pokemonPage, setPokemonPage] = useState({limit:perLoad, offset:perLoad});
    const [pokemonType, setPokemonType] = useState("");
    const [scrollStatus, setScrollStatus] = useState("stop scrolling");
    const [delay, setDelay] = useState(null);
    const [colLength, setColLength] = useState(5);
    window.addEventListener('load', changeGridListCol);
    window.addEventListener('resize', changeGridListCol);
    
    useEffect(() => {
        dispatch(fetchPokemon(perLoad, perLoad));
        dispatch(fetchPokemonType());
    }, [dispatch])

    useEffect(() => {
        function trackScrolling() {
            setScrollStatus("scrolling")
            const wrappedElement = document.getElementById('pokeInfiniteScroll');
            if(isBottom(wrappedElement) && scrollStatus === "stop scrolling"){
                if(pokemonType === ""){
                    loadMore();
                }
            }
            if(delay){
                clearTimeout(delay);
            }
    
            setDelay(setTimeout(() => {
                setScrollStatus("stop scrolling")
            }, 1000))
        }

        function isBottom (el){
            return el.getBoundingClientRect().bottom <= window.innerHeight
        }

        document.addEventListener('scroll', trackScrolling);
        return () => {
            document.removeEventListener('scroll', trackScrolling);
        };
        
    }, [pokemonType])
    
    function loadMore(){
        pokemonPage.offset += pokemonPage.limit;
        setPokemonPage(pokemonPage);
        dispatch(fetchPokemon(pokemonPage.limit, pokemonPage.offset));
    }

    function handlePokemonTypeChange(event){
        if(event.target.value === ""){
            window.location.reload()
        }
        setPokemonType(event.target.value);
        dispatch(fetchPokemon(0, 0, event.target.value));
        // dispatch(insertPockemon([]))
    };

    function changeGridListCol(){
        if(window.innerWidth <= 576){
            setColLength(2)
        }else if(window.innerWidth <= 768){
            setColLength(3)
        }else if(window.innerWidth <= 992){
            setColLength(5)
        }
    }

    function renderContent(){
        let datas = data;
        if(pokemonType !== ""){
            datas = dataByType;
        }

        return datas.map((item, index) => (
            <GridListTile key={index}>
                <div className={(index % colLength !== 0 ? classes.thumbBorder:"")}>
                    <h2>{item.name}</h2>
                    <div>
                        <img src={item.sprites.front_shiny} />
                    </div>
                    <div>
                        {item.type.map((type, index) => {
                            return <b>{(index > 0 ? ",":"")}{type.type.name}</b>
                        })}
                    </div>
                    <div className={classes.bottomF}>
                        <FullScreenDialog buttonTitle="Detail" pokemonName={item.name} data={item.abilities} images={item.sprites} />
                    </div>
                </div>
            </GridListTile>
        ))
    }

    return (
        <>
            <div className={classes.root} id="pokeInfiniteScroll">
                <Backdrop className={classes.backdrop} open={(loading === undefined ? false:loading)}>
                    <CircularProgress color="inherit" />
                </Backdrop>
                <FormControl className={classes.formControl}>
                    <InputLabel id="type-label">Pokemon Type</InputLabel>
                    <Select
                    labelId="type-label"
                    id="types"
                    onChange={handlePokemonTypeChange}
                    >
                        <MenuItem value="">All</MenuItem>
                        {(loadingType === false ? dataType.map((item, index) => {
                            return <MenuItem key={index} value={item.name}>{item.name}</MenuItem>
                        }):<b>Loading Types</b>)}
                    </Select>
                </FormControl>
                <p>{pokemonType}</p>
                <GridList cellHeight={250} cols={colLength}>
                    {renderContent()}
                </GridList>
                {(pokemonType === "" ? <Button variant="contained" color="primary" onClick={() => loadMore()}>Load more</Button>:"")}
                {(loading ? <h3>Loading ...</h3>:"")}
            </div>
        </>
    )
}