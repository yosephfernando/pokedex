import { createSlice } from '@reduxjs/toolkit';
import getPokemonCharacter, {getPokemonCharacterByType} from '../../apiRequest/getPokemonCharacter';
import getPokemonAbilities, {getPokemonAbilitiesEfect} from '../../apiRequest/getPokemonAbilities';

export const pokemonSlice = createSlice({
    name: 'pokemon',
    initialState: {
        data: [],
    },
    reducers: {
        getPokemon: state => {
            state.loading = true
        },
        insertPokemon: (state, { payload }) => {
            state.data = [...state.data, ...payload.results];
            state.dataByType = payload.results
            state.count = payload.count;
            state.next = payload.next;
            state.loading = false
            state.hasErrors = false
        },
        getPokemonFailure: state => {
            state.loading = false
            state.hasErrors = true
        },
    }
});

export const { insertPokemon, getPokemon, getPokemonFailure} = pokemonSlice.actions;

export function fetchPokemon(limit, offset, type="") {
    return async dispatch => {
      dispatch(getPokemon())
      try {
        let mappedData = {results:[], count:0};
        /* get pokemon name */
       
        let response = await getPokemonCharacter(limit, offset);
        if(type !== ""){
            response = await getPokemonCharacterByType(type);
        }

        for(let i = 0;i<(type !== "" ? response.pokemon.length:response.results.length);i++){
             /* get pokemon abilities and picture */
            let abilities = await getPokemonAbilities((type !== "" ? response.pokemon[i].pokemon.name:response.results[i].name));
            let abilitiMapped = [];
            for(let j = 0;j<abilities.abilities.length;j++){
                 /* get pokemon abilities effects */
                let abilitiesEfetct = await getPokemonAbilitiesEfect(abilities.abilities[j].ability.name);
                let effect = "";
                for(let k = 0;k<abilitiesEfetct.effect_entries.length;k++){
                    if(abilitiesEfetct.effect_entries[k].language.name === "en"){
                        effect = abilitiesEfetct.effect_entries[k].short_effect;
                        break;
                    }
                }
                abilitiMapped.push({
                    "name": abilities.abilities[j].ability.name,
                    "effect":  effect
                })
            }
            mappedData.results.push({
                "name": (type !== "" ? response.pokemon[i].pokemon.name:response.results[i].name),
                "sprites": abilities.sprites,
                "type": abilities.types,
                "abilities": abilitiMapped
            });
        }
        
        mappedData.count = response.count;
        const data = mappedData;
        dispatch(insertPokemon(data))
      } catch (error) {
        dispatch(getPokemonFailure())
      }
    }
}

export const pokemonSelector = state => state.pokemon

export default pokemonSlice.reducer;

