import React, {useState} from 'react';
import Grid from '@material-ui/core/Grid';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

export default function AbilityGrid(props) {
    const [pitureFrom, setpictureFrom] = React.useState(props.sprites.front_shiny);

    const handlePictureFrom = (event, newFrom) => {
        setpictureFrom(newFrom);
    };

    return (
        <div style={{padding:10}}>
            <h1 style={{textAlign:"center"}}>Abilities</h1>
            <Grid container justify="center">
                {(props.abilities.length > 0 ? props.abilities.map((item, index) => { 
                    return <Grid item xs={12} sm={6} md={3}>
                        <h1 key={index} style={{marginBottom:0}}>{item.name}</h1>
                        <h3 key={index} style={{marginTop:0}}>{item.effect}</h3>
                    </Grid>
                }):<h3>loading ...</h3>)}
            </Grid>
            <Grid container justify="center">
                <Grid item md={5} style={{textAlign:"center"}}>
                    <div>
                        <img src={pitureFrom} />
                    </div>
                    <div>
                        <ToggleButtonGroup
                            value={pitureFrom}
                            exclusive
                            onChange={handlePictureFrom}
                            aria-label="Picture"
                        >
                            <ToggleButton value={props.sprites.front_shiny} aria-label="Back">
                                <span>front</span>
                            </ToggleButton>
                            <ToggleButton value={props.sprites.back_shiny} aria-label="Front">
                                <span>back</span>
                            </ToggleButton>
                        </ToggleButtonGroup>
                    </div>
                </Grid>
            </Grid>
        </div>
    );
}