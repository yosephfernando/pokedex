import { createSlice } from '@reduxjs/toolkit';
import getPokemonTypeApi from '../../apiRequest/getPokemonType';

export const pokemonTypeSlice = createSlice({
    name: 'pokemonType',
    initialState: {
        dataType: [],
    },
    reducers: {
        getPokemonType: state => {
            state.loadingType = true
        },
        insertPockemonType: (state, { payload }) => {
            state.dataType = [...state.dataType, ...payload.types];
            state.countType = payload.count;
            state.loadingType = false
            state.hasErrorsType = false
        },
        getPokemonTypeFailure: state => {
            state.loadingType = false
            state.hasErrorsType = true
        },
    }
});

export const { insertPockemonType, getPokemonType, getPokemonTypeFailure} = pokemonTypeSlice.actions;

export function fetchPokemonType() {
    return async dispatch => {
        dispatch(getPokemonType())
        try {
            let mappedData = {types:[], count:0};
             /* get pokemon type name */
            let response = await getPokemonTypeApi();
            for(let i =0;i<response.results.length;i++){
                mappedData.types.push(
                    {name: response.results[i].name}
                )
            }
            mappedData.count = response.count;
            const data = mappedData;
            dispatch(insertPockemonType(data))
        }catch (error) {
            dispatch(getPokemonTypeFailure())
        }
    }
}

export const pokemonTypeSelector = state => state.pokemonType

export default pokemonTypeSlice.reducer;