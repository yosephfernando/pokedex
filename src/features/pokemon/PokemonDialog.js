import React, {useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import Slide from '@material-ui/core/Slide';
import AbilityGrid from './PokemonAbilityGrid';

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function FullScreenDialog(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [abilities, setAbilities] = React.useState([]);
  const [sprites, setSprites] = React.useState({});

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    setAbilities(props.data);
    setSprites(props.images)
  })

  return (
    <div key={props.pokemonName}>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        {props.buttonTitle}
      </Button>
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              close
            </IconButton>
          </Toolbar>
        </AppBar>
        <div>
            <AbilityGrid abilities={abilities} sprites={sprites}/>
        </div>
      </Dialog>
    </div>
  );
}
