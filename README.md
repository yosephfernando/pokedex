This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template.

## Online demo
`http://51.79.157.121:7273/`

## Available Scripts
In the project directory, you can run:

### `yarn start`
Runs the app in the development mode.
Open http://localhost:3000 to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `yarn test`
Runs the app test script.
- renders home page
- api fetch 10 data pokemon
- api fetch data pokemon abilities

### `yarn build`


### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

## Install on your local machine ( non docker )
1. First clone this project. `git clone https://gitlab.com/yosephfernando/pokedex.git`
2. Run `yarn install` or `npm install` according to package manager that you installed on your local machine
3. Run `yarn start` or `npm start` to run the pokedex application
4. Open `http://localhost:3000` from your browser to open the pokedex application

## Install on your local machine ( docker )
1. First clone this project by run `git clone https://gitlab.com/yosephfernando/pokedex.git`
2. Run `docker build -t pokedex .` to build your local image
3. Run `docker run --name pokedex -d -p 7273:80 pokedex` to run the pokedex application
4. Open `http://localhost:7273` from your browser to open the pokedex application
